<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth',[App\Http\Controllers\apis\LoginController::class, 'auth']);

Route::middleware('auth:sanctum')->group( function () {
    Route::get('task-list',[App\Http\Controllers\apis\TaskController::class, 'list']);    
    Route::post('task-create',[App\Http\Controllers\apis\TaskController::class, 'create']);    
    Route::post('task-update',[App\Http\Controllers\apis\TaskController::class, 'update']);    

    Route::get('tag-search',[App\Http\Controllers\apis\TagController::class, 'search']);    
    Route::get('user-search',[App\Http\Controllers\apis\UserController::class, 'search']);    
});
