<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskAssignment extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ["user_id","task_id"];

    public function assignee(){
        return $this->belongsTo(User::class,"user_id","id");
    }
}   
