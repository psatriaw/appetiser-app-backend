<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['title','description','due_date','user_id','status','priority'];

    public function tags(){
        return $this->hasMany(TaskTag::class);
    }


    public function assignee(){
        return $this->hasOne(TaskAssignment::class);
    }
    

    public function files(){
        return $this->hasMany(TaskFile::class);
    }
}
