<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskTag extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ["task_id","tag_id"];

    public function tag(){
        return $this->belongsTo(Tag::class);
    }

    public function task(){
        return $this->belongsTo(Task::class);
    }
}
