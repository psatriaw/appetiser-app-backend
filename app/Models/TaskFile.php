<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskFile extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ["task_id","name","path","file_size"];

    public function task(){
        return $this->belongsTo(Task::class);
    }
}
