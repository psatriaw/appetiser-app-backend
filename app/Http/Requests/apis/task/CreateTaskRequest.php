<?php

namespace App\Http\Requests\apis\task;


use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Http\Exceptions\HttpResponseException;

use Illuminate\Contracts\Validation\Validator;

class CreateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return (auth('sanctum')->user())?true:false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */

    public function failedValidation(Validator $validator){

        throw new HttpResponseException(response()->json([

            'status'    => 'error_validation',
            'message'   => 'Validation errors',
            'data'      => $validator->errors()

        ]));

    }

    public function rules(): array
    {
        return [
            "title"              => ["required","max:128"],
            "assign_to"          => ["required"],
            "tags"               => ["required"],
            "priority"           => ["required"],
        ];
    }
}
