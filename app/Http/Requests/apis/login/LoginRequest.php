<?php

namespace App\Http\Requests\apis\login;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Http\Exceptions\HttpResponseException;

use Illuminate\Contracts\Validation\Validator;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function failedValidation(Validator $validator){

        throw new HttpResponseException(response()->json([

            'status'    => 'error_validation',
            'message'   => 'Validation errors',
            'data'      => $validator->errors()

        ]));

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */

    public function rules()
    {
        return [
            "email"           => ["required","email"],
            "password"        => ["required","min:8","regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/"]
        ];
    }

    public function messages(){
        return [
            "password.regex"         => "Password should contains:<br> 
                                        English uppercase characters (A - Z)<br>
                                        English lowercase characters (a - z)<br>
                                        Base 10 digits (0 - 9)<br>
                                        Symbols (For example: !, $, #, or %)<br>",
        ];
    }
}
