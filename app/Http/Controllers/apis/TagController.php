<?php

namespace App\Http\Controllers\apis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Tag;

class TagController extends Controller
{
    public function search(Request $request){
        $keyword = $request->keyword;
        $result = [
            "status"    => "success",
            "message"   => "List of Tag",
            "data"      => Tag::get()
        ];

        return response()->json($result,200);  
    }
}
