<?php

namespace App\Http\Controllers\apis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

class UserController extends Controller
{
    public function search(Request $request){
        $keyword = $request->keyword;
        $result = [
            "status"    => "success",
            "message"   => "List of Tag",
            "data"      => User::where("name",'like',"%$keyword%")->get()
        ];

        return response()->json($result,200);  
    }
}
