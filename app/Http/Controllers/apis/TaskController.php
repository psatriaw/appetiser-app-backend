<?php

namespace App\Http\Controllers\apis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\apis\task\{CreateTaskRequest, UpdateTaskRequest};
use App\Models\{Task,TaskAssignment, TaskTag, TaskFile};

use Illuminate\Support\Facades\File;

use Auth;
use Carbon\Carbon;

class TaskController extends Controller
{
    public function create(CreateTaskRequest $request){
        $user   = auth('sanctum')->user();
        $create = Task::create([
            "title"         => $request->title,
            "description"   => $request->description,
            "due_date"      => $request->due_date,
            "priority"      => $request->priority,
            "user_id"       => $user->id
        ]);
        

        if($create){

            if($request->assign_to){
                TaskAssignment::updateOrCreate([
                    "task_id"   => $create->id
                ],[
                    "user_id"   => $request->assign_to
                ]);
            }
    
            if($request->tags){
                $tasktag = (new TaskTag());
                $tasktag->where("task_id",$create->id)->delete();
                foreach($request->tags as $index=>$tag){
                    $tasktag->create([
                        "tag_id"    => $tag,
                        "task_id"   => $create->id
                    ]);
                }
            }

            $result = [
                "status"    => "success",
                "message"   => "Task successfully crated",
                "data"      => Task::where("id",$create->id)->with('assignee.assignee','tags.tag')->first()
            ];
        }else{
            $result = [
                "status"    => "error",
                "message"   => "Failed to create task"
            ];
        }

        return response()->json($result,200);
    }

    public function update(UpdateTaskRequest $request){
        $user   = auth('sanctum')->user();
        $task   = (new Task())->where("id",$request->id)->with('assignee.assignee','tags.tag');
        $update = $task->update([
            "title"         => $request->title,
            "description"   => $request->description,
            "due_date"      => $request->due_date,
            "priority"      => $request->priority,
            "status"        => $request->status,
        ]);

        if($request->assign_to){
            TaskAssignment::updateOrCreate([
                "task_id"   => $request->id
            ],[
                "user_id"   => $request->assign_to
            ]);
        }

        if($request->tags){
            $tasktag = (new TaskTag());
            $tasktag->where("task_id",$request->id)->delete();
            foreach($request->tags as $index=>$tag){
                if(is_numeric($tag)){
                    $tasktag->create([
                        "tag_id"    => $tag,
                        "task_id"   => $request->id
                    ]);
                }else{
                    $tasktag->create([
                        "tag_id"    => $tag['tag_id'],
                        "task_id"   => $request->id
                    ]);
                }
            }
        }

        if($request->upload_files){
            $thefiles = $request->file('upload_files');
            $upload_path    = public_path('uploads/');

            foreach($thefiles as $index=>$file){
                File::isDirectory($upload_path) or File::makeDirectory($upload_path, 0777, true, true);
    
                $file_name          = $file->getClientOriginalName();
                $size               = $file->getSize();
    
                $generated_new_name = time().'-'.rand(1111,9999).$file_name;
                $file->move($upload_path, $generated_new_name);
    
                $path    = "uploads/".$generated_new_name;
    
                $letter_file        = $path;
                $letter_file_name   = $file_name;

                TaskFile::create([
                    "task_id"   => $request->id,
                    "name"      => $file_name,
                    "file_size" => $size,
                    "path"      => $path
                ]);
            }

        }else{
            $files = null;
        }

        if($update){
            $result = [
                "status"    => "success",
                "message"   => "Task successfully updated",
                "data"      => Task::where("id",$request->id)->with('assignee.assignee','tags.tag','files')->first(),
            ];
        }else{
            $result = [
                "status"    => "error",
                "message"   => "Failed to update task"
            ];
        }

        return response()->json($result,200);
    }


    public function list(Request $request){
        $user   = auth('sanctum')->user();
        $perPage        = $request->perPage;
        $currentPage    = $request->currentPage;
        $keyword        = $request->keyword;
        $filter         = $request->filter;
        $date           = $request->date;

        $tasks          = new Task();

        if(@$filter['status']){
            $tasks      = $tasks->whereIn("status",$filter['status']);

            if(!in_array('archieved',$filter['status'])){
                $tasks  = $tasks->whereNotIn("status",["archieved"]);
            }
        }

        if($keyword!=""){
            $tasks  = $tasks->where("title",'like',"%$keyword%");
        }

        if(@$filter['date']){
            $filter['date'][0] = (new Carbon(@$filter['date'][0]))->toDateString();
            $filter['date'][1] = (new Carbon(@$filter['date'][1]))->toDateString();

            if($filter['date'][0]!=$filter['date'][1] && $filter['date'][0]!=date("Y-m-d") && @$filter['date'][0] && @$filter['date'][1]){
                $tasks      = $tasks->whereBetween("due_date",$filter['date']);
            }
        }

        $total          = $tasks->count();

        $tasks          = $tasks->where("user_id",$user->id)->with('assignee.assignee','tags.tag','files');
        
        $tasks          = $tasks->limit($perPage)->skip(($currentPage-1)*$perPage);

        if(@$filter['sort_by']){
            $tasks      = $tasks->orderBy($filter['sort_by'],"DESC");
        }
        
        $tasks          = $tasks->get();

        

        if($tasks){
            $result = [
                "status"    => "success",
                "data"      => $tasks,
                "total"     => $total,
                "filter"    => $filter
            ];
        }else{
            $result = [
                "status"    => "error",
                "message"   => "No data"
            ];
        }

        return response()->json($result,200);
    }
}
