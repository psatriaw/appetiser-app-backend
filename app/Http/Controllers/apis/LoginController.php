<?php

namespace App\Http\Controllers\apis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests\apis\login\{ LoginRequest };

use App\Models\{User};

class LoginController extends Controller
{
    public function auth(LoginRequest $request){

        $user = User::where("email",$request->email)->first();

        if(Hash::check($request->password,$user->password)){
            $token  = $user->createToken("auth_token")->plainTextToken;
            $result = [
                "status"    => "success",
                'user'      => $user,
                'token'     => $token, 
                'token_type' => "Bearer"
            ];
        }else{
            $result = [
                "status"    => "error",
                "message"   => "Email and password combination not match!",
                "password"  => Hash::make($request->password)
            ];
        }
    
        return response()->json($result,200);
    }
}
