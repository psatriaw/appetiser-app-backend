<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\{Tag};

class CreateSampleTags extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Tag::create([
            "name"          => "Design",
            "permalink"     => "design"
        ]);

        Tag::create([
            "name"          => "DevOps",
            "permalink"     => "devops"
        ]);

        Tag::create([
            "name"          => "Maintenance",
            "permalink"     => "maintenance"
        ]);
    }
}
